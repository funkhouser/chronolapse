FILENAME=$1
FRAMES=$2

if [ -z "$FRAMES" ]; then
  FRAMES=10
fi


EXTENSION="${FILENAME##*.}"
RAW_FILENAME="${FILENAME%.*}"

for ((i=1;i<=FRAMES;i++)); do
  OUTPUT="${RAW_FILENAME}${i}.${EXTENSION}"
  PNG_OUTPUT="${RAW_FILENAME}${i}.png"
  webpmux -get frame $i "$FILENAME" -o "$OUTPUT"
  dwebp "$OUTPUT" -o "$PNG_OUTPUT"
done
