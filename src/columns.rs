// Each file is a `mod`. I feel like that fact was not articulated well within the 'modules' part.
// pinnacles_col0.jpg
// /Users/kevin.funkhouser/Pictures/a6000/Pinnacles
// 5
// DSC{5}.jpg

// pub mod columns {
use rayon::prelude::*;

use rand::{thread_rng, Rng};
use std::env;
use std::fs;
use std::process;
use std::slice::Iter;
use image::{DynamicImage, ImageError, ImageResult, GenericImageView, GenericImage, Rgba, Pixel, RgbaImage};
use std::cmp::min;
use std::collections::HashMap;
use std::env::join_paths;
use std::ffi::OsString;
use std::path::Path;

use Config;

// const offset: i32 = 7450;
// const offset: i32 = 7243;
const offset: i32 = 1000;
const image_count: i32 = 50;
const skip: i32 = 200;
// const image_count: i32 = 50;
// const column_count: i32 = 20; // Maybe this will average out the columns or something.

// const width: u32 = 6000;
// const height: u32 = 4000;
const width: u32 = 1920;
const height: u32 = 1080;

const random: bool = false;

pub fn apply(config: &Config) -> String {
    let base_path = Path::new(&config.input_directory);
    let mut output_image = RgbaImage::new(width, height);

    // Probably could be done more cleanly, but this will load the original image as base_image
    // In the column case this is probably not necessary at all.
    // let filename = create_file_name(&config, offset);
    // let filename_path = Path::new(&filename);
    // let base_file_path = base_path.join(&filename_path);
    // let jpg_path = base_file_path.as_path();
    // let base_image = image::open(jpg_path).unwrap();

    // for x in 0..width {
    //     for y in 0..height {
    //         let mut pixel = output_image.get_pixel(x, y);
    //         let base_pixel = base_image.get_pixel(x, y);
    //         // let transparent_pixel: Rgba<u8> = Rgba([0x8f, 0x8f, 0x8f, 0x8f]);
    //         output_image.put_pixel(x, y, transparent_pixel);
    //         let transparent_pixel: Rgba<u8> = Rgba([base_pixel[0], base_pixel[1], base_pixel[2], base_pixel[3]]);
    //         output_image.put_pixel(x, y, transparent_pixel);
    //     }
    // }

    let mut images: Vec<DynamicImage> = Vec::new();
    let col_size: u32 = width / (image_count as u32);
    let column_mappings = create_column_mappings();
    (0..image_count).for_each(|j| {
        let i = offset + j * skip;
        let filename = create_file_name(&config, &i);
        println!("Merging image: {}", filename);
        let filename_path = Path::new(&filename);
        let base_file_path = base_path.join(&filename_path);
        let jpg_path = base_file_path.as_path();
        let image = image::open(jpg_path).unwrap();

        // let x_start: u32 = col_size * (j as u32);
        // let x_end: u32 = col_size * ((j as u32) + 1);
        let index: i32 = column_mappings[&j];
        let x_start: u32 = col_size * (index as u32);
        let x_end: u32 = x_start + col_size;
        (x_start..x_end).for_each(|x|
            (0..height).for_each(|y| {
                let pixel: Rgba<u8> = image.get_pixel(x, y);
                let base_pixel = output_image.get_pixel(x, y);
                // let transparent_pixel: Rgba<u8> = Rgba([pixel.data[0], pixel.data[1], pixel.data[2], 0x8f]);
                // TODO Different algorithms for merging the two pixels should be available as parameters.
                let mixed_pixel: Rgba<u8> = Rgba([
                    pixel[0],
                    pixel[1],
                    pixel[2],
                    pixel[3]
                ]);
                output_image.put_pixel(x, y, mixed_pixel);
            })
        );
    });

    println!("Saved output to {}", config.filename);
    output_image.save(&config.filename);

    "Success".to_string()
}

fn create_column_mappings() -> HashMap<i32, i32> {
    let mut col_mappings: HashMap<i32, i32> = HashMap::new();

    if random {
        (0..image_count).for_each(|i| {
            col_mappings.insert(i, i);
        });
    } else {
        let mut rng = thread_rng();
        (0..image_count).for_each(|i| {
            let ic: i32 = image_count.clone();
            let mut index: i32 = rng.gen_range(0..ic);
            while col_mappings.contains_key(&index) {
                index = (index + 1) % ic;
            }
            col_mappings.insert(index, i);
        });
    }

    col_mappings
}

fn create_file_name(config: &Config, current_offset: &i32) -> String {
    let filename;
    let prefix: String = "".to_string();
    if config.file_name_digits == 3 {
        filename = format!("{}/{}{:03}.jpg", config.input_directory, prefix, current_offset);
    } else if config.file_name_digits == 4 {
        filename = format!("{}/{}{:04}.jpg", config.input_directory, prefix, current_offset);
    } else if config.file_name_digits == 5 {
        filename = format!("{}/{}{:05}.jpg", config.input_directory, prefix, current_offset);
    } else {
        filename = format!("{}/{}{:06}.jpg", config.input_directory, prefix, current_offset);
    }
    filename
}
// }
