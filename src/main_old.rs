extern crate photon_rs;
extern crate image;

use photon_rs::PhotonImage;
use photon_rs::native::{open_image, save_image};

use std::env;
use std::fs;
use std::process;
use std::slice::Iter;
use image::{DynamicImage, ImageError, ImageResult, GenericImageView, GenericImage, Rgba, Pixel, RgbaImage};
use std::cmp::min;
use std::env::join_paths;
use std::ffi::OsString;
use std::path::Path;


fn main() {
    print_splash_text();

    // Args essentially start at 1 because the 0th index
    // is the run target itself (i.e. chronolapse)
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Exactly two arguments required: filename and operation");
        process::exit(1);
    }
    let config: Config = parse_config(&args).expect(format!("Invalid configuration: output_filename={} operation={}", args[1], args[2]).as_ref());
    let base_path = Path::new("/mnt/e/Media/Pictures/a6000/2021-11-13/25_output");
    // let base_path = Path::new("/mnt/c/Users/Kevin/2021-11-13/32_output");
    // let base_filepath: OsString = join_paths(base_paths.iter()).expect("Path creation failed");

    let offset = 500;
    let image_count = 3500;

    let mut images: Vec<DynamicImage> = Vec::new();
    let skip = 3;
    for i in offset..(image_count + offset) {
        if i % 10 == 0 {
            println!("Loading image: {:04}", i);
        }
        if i % skip == 0 {
            let filename = format!("/mnt/e/Media/Pictures/a6000/2021-11-13/25_output/{:03}.jpg", i);
            let filename_path =  Path::new(&filename);
            let base_file_path = base_path.join(&filename_path);
            let jpg_path = base_file_path.as_path();
            // let file_path_ref = base_filepath.as_ref();

            // let filepath = join_paths([file_path_ref, filename_path].iter()).expect("Path creation failed here");
            // let filepath = join_paths([Path::new("/mnt"), Path::new("/e"), Path::new("/Media"), Path::new("/Pictures"), Path::new("/a6000"), Path::new("/2020-3-8"), Path::new(&filename)].iter()).expect("Path creation failed here");
            // println!("{:?}", &jpg_path);
            images.push(image::open(jpg_path).unwrap());
        }
        // let filepath = join_paths(["resources", "harbor", format!("output{:03}.jpg", i)]).expect("Path creation failed");
        // let filename = format!("/mnt/e/Media/Pictures/a6000/2021-11-13/32_output/output32_{:03}.jpg", i);
    }


    // let images = [
    //     image::open("resources/monarchs0/monarchs02.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs03.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs04.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs05.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs06.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs07.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs08.png").unwrap(),
    //     image::open("resources/monarchs0/monarchs09.png").unwrap(),
    // ];

    let width = 1920;
    let height = 1080;

    // TODO This is one way to instantiate, but it doesn't appear necessary
    // let mut raw_image: DynamicImage = DynamicImage::new_bgra8(width, height);
    // let mut output_image: DynamicImage = image::open("resources/monarchs0/monarchs01.png").unwrap();
    // let output_image_path = Path::new("/mnt/e/Media/Pictures/a6000/2021-11-13/32_output/chrono_001.jpg");
    // let output_image_path = join_paths([base_filepath.as_ref(), Path::new("output34_0001.jpg")].iter()).expect("Again path creation failed");
    // let base_image: DynamicImage = image::open(output_image_path).unwrap();
    let mut output_image = RgbaImage::new(width, height);
    let base_image = &images[0];
    for x in 0..width {
        for y in 0..height {
            let mut pixel = output_image.get_pixel(x, y);
            let base_pixel = base_image.get_pixel(x, y);
            // let transparent_pixel: Rgba<u8> = Rgba([0x8f, 0x8f, 0x8f, 0x8f]);
            let transparent_pixel: Rgba<u8> = Rgba([base_pixel[0], base_pixel[1], base_pixel[2], base_pixel[3]]);
            output_image.put_pixel(x, y, transparent_pixel);
        }
    }

    let mut image_index = 0;
    for image in images.iter() {
        image_index += 1;
        if image_index % 10 == 0 {
            println!("Merging {:04}", image_index);

        }
        // let pixels = image.pixels();
        for y in 0..height {
            for x in 0..width {
                // Deprecated but get_pixel_mut is unimplemented on DynamicImage (is implemented on Buffer). Okay.

                // Blend links to stackoverflow.com/questions/7438263/alpha-compositing-algorithm-blend-modes#answer-11163848
                let pixel: Rgba<u8> = image.get_pixel(x, y);
                let base_pixel = output_image.get_pixel(x, y);
                // let transparent_pixel: Rgba<u8> = Rgba([pixel.data[0], pixel.data[1], pixel.data[2], 0x8f]);


                // TODO Different algorithms for merging the two pixels should be available as parameters.
                let mixed_pixel: Rgba<u8> = Rgba([
                    min(pixel[0], base_pixel[0]),
                    min(pixel[1], base_pixel[1]),
                    min(pixel[2], base_pixel[2]),
                    min(pixel[3], base_pixel[3]),
                ]);

                output_image.put_pixel(x, y, mixed_pixel);
            }
        }
    }

    output_image.save(config.filename);
}

struct Config {
    filename: String,
    operation: Operation,
}

fn print_splash_text() {
    let splash_contents = fs::read_to_string("resources/splash.txt")
        .expect("Error reading splash text file");
    println!("{}", splash_contents);
}

// Clone really shouldn't be used outside of small strings
// due to the performance hit it incurs. In this case
// it is reasonable but will not be for handling the image files
// or matrices for example.

// In short, clone creates a full copy for the instance to own
fn parse_config(args: &[String]) -> Result<Config, ()> {
    let filename = args[1].clone();
    // let operation = args[2].clone();
    // let operation = args[2].parse::<Operation>();
    let operation: Operation = args[2].parse()?;

    Ok(Config { filename, operation })
}


pub enum Operation {
    Read,
    Write,
}

impl Operation {
    pub fn iterator() -> Iter<'static, Operation> {
        static OPERATIONS: [Operation; 1] = [Operation::Read];
        OPERATIONS.iter()
    }
}

impl std::str::FromStr for Operation {
    type Err = ();

    fn from_str(s: &str) -> Result<Operation, ()> {
        match s {
            "read" => Ok(Operation::Read),
            "write" => Ok(Operation::Write),
            _ => Err(()),
        }
    }
}

