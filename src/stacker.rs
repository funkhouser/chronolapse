pub mod stacker {

use rayon::prelude::*;

use std::env;
use std::fs;
use std::process;
use std::slice::Iter;
use image::{DynamicImage, ImageError, ImageResult, GenericImageView, GenericImage, Rgba, Pixel, RgbaImage};
use std::cmp::min;
use std::env::join_paths;
use std::ffi::OsString;
use std::path::Path;

use Config;

    const offset: i32 = 500;
    const image_count: i32 = 50;

    const width: u32 = 1920;
    const height: u32 = 1080;

    pub fn apply(config: &Config) -> String {
         let base_path = Path::new(&config.input_directory);
            // let base_path = Path::new("/mnt/c/Users/Kevin/2021-11-13/32_output");
            // let base_filepath: OsString = join_paths(base_paths.iter()).expect("Path creation failed");

        let mut output_image = RgbaImage::new(width, height);

        // Probably could be done more cleanly, but this will load the original image as base_image
        let filename = create_file_name(&config, offset);
        let filename_path =  Path::new(&filename);
        let base_file_path = base_path.join(&filename_path);
        let jpg_path = base_file_path.as_path();
        let base_image = image::open(jpg_path).unwrap();

        for x in 0..width {
            for y in 0..height {
                let mut pixel = output_image.get_pixel(x, y);
                let base_pixel = base_image.get_pixel(x, y);
                // let transparent_pixel: Rgba<u8> = Rgba([0x8f, 0x8f, 0x8f, 0x8f]);
                let transparent_pixel: Rgba<u8> = Rgba([base_pixel[0], base_pixel[1], base_pixel[2], base_pixel[3]]);
                output_image.put_pixel(x, y, transparent_pixel);
            }
        }

        let mut images: Vec<DynamicImage> = Vec::new();
        let skip = 1;
        (offset..(image_count + offset)).for_each(|i|
            if i % skip == 0 {
                let filename = create_file_name(&config, i);
                println!("Merging image: {}", filename);
                let filename_path =  Path::new(&filename);
                let base_file_path = base_path.join(&filename_path);
                let jpg_path = base_file_path.as_path();
                let image = image::open(jpg_path).unwrap();
                (0..height).for_each(|y|
                    (0..width).for_each(|x| {
                        let pixel: Rgba<u8> = image.get_pixel(x, y);
                        let base_pixel = output_image.get_pixel(x, y);
                        // let transparent_pixel: Rgba<u8> = Rgba([pixel.data[0], pixel.data[1], pixel.data[2], 0x8f]);
                        // TODO Different algorithms for merging the two pixels should be available as parameters.
                        let mixed_pixel: Rgba<u8> = Rgba([
                            min(pixel[0], base_pixel[0]),
                            min(pixel[1], base_pixel[1]),
                            min(pixel[2], base_pixel[2]),
                            min(pixel[3], base_pixel[3]),
                        ]);
                        output_image.put_pixel(x, y, mixed_pixel);
                    })
                )
            }
        );

        println!("Saved output to {}", config.filename);
        output_image.save(&config.filename);

        "Success".to_string()
    }

    fn create_file_name(config: &Config, current_offset: i32) -> String {
        let filename;
        if config.file_name_digits == 3 {
            filename = format!("{}/{:03}.jpg", config.input_directory, &current_offset);
        } else if config.file_name_digits == 4 {
            filename = format!("{}/{:04}.jpg", config.input_directory, &current_offset);
        } else if config.file_name_digits == 5 {
            filename = format!("{}/{:05}.jpg", config.input_directory, &current_offset);
        } else {
            filename = format!("{}/{:06}.jpg", config.input_directory, &current_offset);
        }
        filename
    }
}
