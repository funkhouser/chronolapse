extern crate photon_rs;
extern crate image;
extern crate rand;
extern crate rayon;

use photon_rs::PhotonImage;
use photon_rs::native::{open_image, save_image};

use rayon::prelude::*;

use std::env;
use std::fs;
use std::process;
use std::slice::Iter;
use image::{DynamicImage, ImageError, ImageResult, GenericImageView, GenericImage, Rgba, Pixel, RgbaImage};
use std::cmp::min;
use std::env::join_paths;
use std::ffi::OsString;
use std::path::Path;

mod stacker;
use stacker::stacker::apply;

mod columns;

use columns::apply as column_apply;

fn main() {
    print_splash_text();

    // Its simpler to set these up right away rather than create too many inputs
    let offset = 500;
    let image_count = 50;

    // Args essentially start at 1 because the 0th index
    // is the run target itself (i.e. chronolapse)
    let args: Vec<String> = env::args().collect();
    if args.len() != 4 {
        println!("Exactly three arguments required: output_filename, input_directory, and file_name_digits");
        process::exit(1);
    }
    let config: Config = parse_config(&args).expect(format!("Invalid configuration: output_filename={} input_directory={} file_name_digits={}", args[1], args[2], args[3]).as_ref());

   // apply(&config);
    column_apply(&config);
}



pub struct Config {
    filename: String,
    input_directory: String,
    file_name_digits: u8,
}

fn print_splash_text() {
    let splash_contents = fs::read_to_string("resources/splash.txt")
        .expect("Error reading splash text file");
    println!("{}", splash_contents);
}

// Clone really shouldn't be used outside of small strings
// due to the performance hit it incurs. In this case
// it is reasonable but will not be for handling the image files
// or matrices for example.

// In short, clone creates a full copy for the instance to own
fn parse_config(args: &[String]) -> Result<Config, ()> {
    let filename = args[1].clone();
    // let operation = args[2].clone();
    // let operation = args[2].parse::<Operation>();
    // let operation: Operation = args[2].parse()?;
    let input_directory = args[2].clone();
    let file_name_digits = u8::from_str_radix(&args[3].clone(), 10).unwrap();

    Ok(Config { filename, input_directory, file_name_digits })
}

