# Chronolapse

Inspired by projects such as 
https://www.nationalgeographic.com/magazine/2018/01/photo-journal-birds-paths-migration-starling/
this project seeks to perform image processing and pattern recognition to transform
series of images into novel forms. The initial target will be a high frequency delta
creator to track things moving akin to that link and attempting
to pattern them together to create surreal images.



## Potential Solution One: Standard Edge Detection
Following edge detection (likely via the fuzzy matching) an additional step to determine where in the image an object moved
can be used to trace the movement of the object across the viewport.

## Potential Solution Two: Naivety
Another option would be to attempt to do a different blending algorithm. Namely an algo that instead of allowing brightness to be
the primary result of consolidating a large amount of pixel values instead have the darkest frame or maybe the lightest pixel within
the darkest sequence of pixels for instance (or maybe in some way guaranteeing that nearby pixels are using the same frame if they have
similar light/dark patterns)

This can most simply be seen as stacking all of the frames together in a 3d matrix rather than a simple 2d one.



## General Tooling`

For webp videos/images they can be extracted via `webpmux`. Additionally `webpinfo` can be used to inspect further.

```
webpmux -frame
```
This process is handled somewhat automatically in `./extract_frames.sh` but it is a naive extraction and won't
necessarily grab all of the images.



For other video media `ffmpeg` is the tool of choice. For instance to extract from the MTS file. The qscale
is slightly unclear, but 0 is for lossless (if the video itself is lossless) and generally 2-5 seems recommended.
```
ffmpeg -i 00004.MTS -qscale:v 2 output_%03d.jpg
```

or
```shell
for file in *.MTS; do mkdir "${file%.*}_output"; ffmpeg -i "$file" -qscale:v 2 "${file%.*}_output/%04d.jpg"; done
```

### Running
```shell
cargo run
```